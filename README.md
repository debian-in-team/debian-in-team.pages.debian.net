# Debian India Community's homepage.

This is a simple website built using the static site generator
[Hugo](https://gohugo.io/) using the
[Gruvhugo](https://gitlab.com/debian-in/gruvhugo) theme.

- Build Requirements

1. hugo
2. git

### Local development

After cloning this repository using git

```sh
# Creating a blog post
hugo new post/title-of-your-post.md

# live preview (includes draft pages, without -D flag hides draft pages)
hugo server -D
```

### Embedding images in markdown files

We can use hugo's image shortcodes to embed images in markdown files.

A simple example:

`{{< figure src="elephant.jpg" title="An elephant at sunset" >}}`

For more info & customizations, Refer this [link](https://gohugo.io/content-management/shortcodes/#figure)

> The website is developed on [Gitlab](https://gitlab.com/debian-in/debian-in.gitlab.io) and a mirror is maintained on [Debian
> Salsa](https://salsa.debian.org/debian-in-team/debian-in-team.pages.debian.net)

<!-- **If you would like to open a issue or merge request please do so on salsa.** -->
